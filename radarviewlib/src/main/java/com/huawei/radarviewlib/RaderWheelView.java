package com.huawei.radarviewlib;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.app.dispatcher.TaskDispatcher;

/**
 * RaderWheelView
 *
 * @author jjy
 * @since 2021-04-23
 */
public class RaderWheelView extends StackLayout implements Component.DrawTask {
    private Context mContext;
    private int mWidth;
    private int mHeight;
    private Paint mPaintLine;
    private int bigR = 100;
    private int smallR = 60;
    private int stoke = 7;
    private int lineWidth = 3;
    private boolean stopRoate = false;
    private RectFloat rectF;
    private TaskDispatcher globalTaskDispatcher;
    private Runnable runnable;

    /**
     * 构造函数
     *
     * @param context context
     */
    public RaderWheelView(Context context) {
        super(context);
        mContext = context.getApplicationContext();
        initPaint();
    }

    /**
     * 构造函数
     *
     * @param context context对象
     * @param attrs 属性参数
     */
    public RaderWheelView(Context context, AttrSet attrs) {
        super(context, attrs);
        mContext = context.getApplicationContext();
        initPaint();
        this.addDrawTask(this::onDraw);
    }

    private void initRadius() {
        bigR = Utils.dip2px(mContext, bigR);
        smallR = Utils.dip2px(mContext, smallR);
        stoke = Utils.dip2px(mContext, stoke);
        lineWidth = Utils.px2dip(mContext, lineWidth);
    }

    private void initPaint() {
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(Color.TRANSPARENT.getValue()));
        setBackground(element);
        initRadius();
        mPaintLine = new Paint();
        mPaintLine.setStrokeWidth(lineWidth);
        mPaintLine.setAntiAlias(true);
        mPaintLine.setStyle(Paint.Style.STROKE_STYLE);
        mPaintLine.setColor(new Color(0x60FFFFFF));
    }

    /**
     * 停止线程
     */
    public void stop() {
        stopRoate = true;
        invalidate();
    }

    @Override
    public void invalidate() {
        super.invalidate();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mWidth = component.getWidth();
        mHeight = component.getHeight();
        if (!stopRoate) { // 停止转圈的时候，隐藏掉中间的小圈
            canvas.drawCircle(mWidth / 2, mHeight / 2, smallR, mPaintLine);
            canvas.drawLine(mWidth / 2 - smallR, mHeight / 2, mWidth / 2 + smallR, mHeight / 2, mPaintLine);
            canvas.drawLine(mWidth / 2, mHeight / 2 - smallR, mWidth / 2, mHeight / 2 + smallR, mPaintLine);
        }
        canvas.drawCircle(mWidth / 2, mHeight / 2, bigR, mPaintLine);
    }

    private void reInvalidate() {
        if (mContext == null) {
            return;
        }
        globalTaskDispatcher = mContext.getMainTaskDispatcher();
        runnable = new Runnable() {
            @Override
            public void run() {
                invalidate();
            }
        };
        globalTaskDispatcher.asyncDispatch(runnable);
    }
}

