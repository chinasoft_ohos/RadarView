/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.radarviewlib;

import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.Optional;

/**
 * Utils
 *
 * @author jjy
 * @since 2021-04-23
 */
public class Utils {
    private static final float HARF_VALUE = 0.5f;

    private Utils() {
    }

    /**
     * 获取屏幕密度
     *
     * @param context context对象
     * @return 屏幕密度
     */
    public static DisplayAttributes getScreenPiex(Context context) {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes displayAttributes = display.get().getAttributes();
        return displayAttributes;
    }

    /**
     * vp转px
     *
     * @param context context对象
     * @param dipValue dip值
     * @return px像素
     */
    public static int dip2px(Context context, float dipValue) {
        DisplayAttributes displayAttributes = getScreenPiex(context);
        return (int) (dipValue * displayAttributes.scalDensity);
    }

    /**
     * px转vp
     *
     * @param context context对象
     * @param pixValue px值
     * @return vp值
     */
    public static int px2dip(Context context, float pixValue) {
        DisplayAttributes displayAttributes = getScreenPiex(context);
        return (int) (pixValue / displayAttributes.scalDensity + HARF_VALUE);
    }
}
