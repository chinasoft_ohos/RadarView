package com.huawei.radarviewlib;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.app.dispatcher.TaskDispatcher;

/**
 * RadarView
 *
 * @author jjy
 * @since 2021-04-23
 */
public class RadarView extends StackLayout implements Component.DrawTask {
    public final static int CLOCK_WISE = 1;
    public final static int ANTI_CLOCK_WISE = -1;
    // 默认为顺时针呢
    private final static int DEFAULT_DIERCTION = CLOCK_WISE;

    // 设定雷达扫描方向
    private int direction = DEFAULT_DIERCTION;
    private boolean threadRunning = true;
    private Context mContext;
    private int mWidth;
    private int mHeight;
    private Paint mPaintLine;
    private Paint mPaintSector;
    private Paint mPaintArc;
    private Paint mPaintArcLine;
    public boolean isstart = false;
    private boolean isStop = false;
    private ScanThread mThread;
    private Paint mPaintPoint;
    //旋转效果起始角度
    private int start = 0;
    private int Istart = 0;
    private int bigR = 100;
    private int smallR = 60;
    private int stoke = 7;
    private int lineWidth = 1;

    private int[] point_x;
    private TaskDispatcher globalTaskDispatcher;
    private Runnable runnable;

    public void stopThread() {
        stop();
        if(mThread != null) {
            mThread = null;
        }
    }

    public void setStart(int start) {
        this.Istart = start;
        System.out.println("start: " + start);
    }

    private int[] point_y;

    private Matrix matrix = new Matrix();
    private boolean stopRoate = false;

    private int codeangel = 0;

    RectFloat rectF;

    public void setStopRoate(boolean stopRoate) {
        System.out.println("stopRoate : " + stopRoate);
        this.stopRoate = stopRoate;
    }

    public void setCodeangel(int codeangel) {
        this.codeangel = codeangel;
    }

    @IntDef({CLOCK_WISE, ANTI_CLOCK_WISE})
    public @interface RADAR_DIRECTION {

    }


    public RadarView(Context context, AttrSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initPaint();
        this.addDrawTask(this::onDraw);
    }

    public RadarView(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
        mContext = context;
        initPaint();
    }

    private void initRadius() {
        bigR = Utils.dip2px(mContext, bigR);
        smallR = Utils.dip2px(mContext, smallR);
        stoke = Utils.dip2px(mContext, stoke);
        lineWidth = Utils.px2dip(mContext, lineWidth);
    }

    private void initPaint() {
        // TODO Auto-generated method stub
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(Color.TRANSPARENT.getValue()));
        setBackground(element);
        initRadius();
        // 宽度=1，抗锯齿，描边效果的白色画笔
        mPaintLine = new Paint();
        mPaintLine.setStrokeWidth(lineWidth);
        mPaintLine.setAntiAlias(true);
        mPaintLine.setStyle(Paint.Style.STROKE_STYLE);
        mPaintLine.setColor(new Color(0x40FFFFFF));

        // 画扇形的画笔
        mPaintSector = new Paint();

        mPaintSector.setAntiAlias(true);
        RadialShader radialGradient = new RadialShader(new Point(getWidth() / 2, getHeight() / 2), bigR
                , new float[]{0.0f, 1f},new Color[]{Color.TRANSPARENT,
                new Color(0x1eFFFFFF)}, Shader.TileMode.CLAMP_TILEMODE);
        mPaintSector.setShader(radialGradient, Paint.ShaderType.GROUP_SHADER);

        // 画白色打分宽圆弧
        mPaintArc = new Paint();
        mPaintArc.setAntiAlias(true);
        mPaintArc.setStrokeWidth(stoke);
        mPaintArc.setStyle(Paint.Style.STROKE_STYLE);

        // 绘制旋转圆弧上的亮线
        mPaintArcLine = new Paint();
        mPaintArcLine.setAntiAlias(true);
        mPaintArcLine.setColor(new Color(0x7affffff));
        mPaintArcLine.setStyle(Paint.Style.STROKE_STYLE);
        mPaintArcLine.setStrokeWidth(lineWidth);

    }

    public void start() {
        mThread = new ScanThread();
        mThread.setName("radar");
        mThread.start();
        threadRunning = true;
        isstart = true;
    }

    public void stop() {
        if (isstart && !isStop) {
            System.out.println("threadRunning stop");
            threadRunning = false;
            isstart = false;
            mContext = null;
            isStop = true;
        }
    }


    @Override
    public void invalidate() {
        super.invalidate();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mWidth = component.getWidth();
        mHeight = component.getHeight();
        if (stopRoate) { // 停止转圈的时候，开始绘制打分进度先绘制一个浅色的圆，然后再绘制一个进度的圆弧
            rectF = new RectFloat(mWidth / 2 - bigR + 20, mHeight / 2 - bigR + 20, mWidth / 2 + bigR - 20, mHeight / 2 + bigR - 20);
            System.out.println("threadRunning Istart : " +  Istart);
            if (Istart <= 360) {
                mPaintArc.setColor(new Color(0x7aFFFFFF));
                Arc arc = new Arc( -90, Istart, false);
                canvas.drawArc(rectF, arc, mPaintArc);
            } else if (Istart > 360 && Istart <= 360 + codeangel) {
                mPaintArc.setColor(new Color(0x7aFFFFFF));
                Arc arc = new Arc( -90, 360, false);
                canvas.drawArc(rectF, arc, mPaintArc);
                mPaintArc.setColor(new Color(0xc1FFFFFF));
                Arc arcTwo = new Arc( -90, Istart - 360, false);
                canvas.drawArc(rectF, arcTwo, mPaintArc);
            } else {
                mPaintArc.setColor(new Color(0x7aFFFFFF));
                Arc arc = new Arc(  -90, 360, false);
                canvas.drawArc(rectF, arc, mPaintArc);
                mPaintArc.setColor(new Color(0xc1FFFFFF));
                Arc arcTwo = new Arc( -90, codeangel, false);
                canvas.drawArc(rectF, arcTwo, mPaintArc);
            }
        }

        if (!stopRoate) {
            // 根据matrix中设定角度，不断绘制shader,呈现出一种扇形扫描效果
            canvas.save();
            int sweep = 1;
            canvas.rotate(start, mWidth / 2, mHeight / 2);
            RectFloat rect = new RectFloat(mWidth / 2 - bigR, mHeight / 2 - bigR, mWidth / 2 + bigR, mHeight / 2 + bigR);
            Arc arc = new Arc(  0, 90, true);
            canvas.drawArc(rect, arc, mPaintSector);
            Arc arcTwo = new Arc( 0, 90, false);
            canvas.drawArc(rect, arcTwo, mPaintArcLine);
            canvas.restore();
            canvas.setMatrix(matrix);
        }
    }

    public void setDirection(@RADAR_DIRECTION int direction) {
        if (direction != CLOCK_WISE && direction != ANTI_CLOCK_WISE) {
            throw new IllegalArgumentException("Use @RADAR_DIRECTION constants only!");
        }
        this.direction = direction;
    }

    protected class ScanThread extends Thread {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            while (threadRunning) {
                if (isstart) {
                    start = start + 5;
                    Istart= Istart + 5;
                    matrix = new Matrix();
                    matrix.preRotate(direction * start, mWidth / 2, mHeight / 2);
                    reInvalidate();
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void reInvalidate() {
        if (mContext == null)return;
        globalTaskDispatcher = mContext.getMainTaskDispatcher();
        runnable = new Runnable() {
            @Override
            public void run() {
                invalidate();
            }
        };
        globalTaskDispatcher.asyncDispatch(runnable);
    }
}
