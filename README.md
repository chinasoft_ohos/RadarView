﻿# RadarView

#### 项目介绍
- 项目名称：RadarView
- 所属系列：openharmony的第三方组件适配移植
- 功能：实现360清理动画
- 项目移植状态：主功能完成
- 调用差异：无
- 基线版本 master分支
- 开发版本：sdk6，DevEco Studio2.2 Beta1

#### 效果演示

<img src="gif/screen.gif"></img> 

#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```
 allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
 }
```
2.在entry模块的build.gradle文件中，
 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:radarvie:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio2.2 Bate1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

此效果主要是通过自定义view，设置属性动画形成的效果。

1、XML中引用控件:

```xml
   <com.huawei.radarviewlib.RadarCommpent
       ohos:id="$+id:radarview"
       ohos:height="match_content"
       ohos:width="match_content"
       app:topColor="0xff15B96B"
       app:middColor="0xffF29336"
       app:lowColor="0xffEC6066"
       >
   </com.huawei.radarviewlib.RadarCommpent>
```

2、启动动画:

```java
 CHANNEL_ID 目前demo中设置的是12
 RadarCommpent radarCommpent = (RadarCommpent) findComponentById(ResourceTable.Id_radarview);
 radarCommpent.showCheckProcess(CHANNEL_ID);
```

自定义MaterialSpinner控件，可以添加自定义属性，具体可用属性如下:

| name                    | type      | info                                                   |
|-------------------------|-----------|--------------------------------------------------------|
| topColor                | int       | 检测值是90%-100%的颜色                                 |
| middColor               | int       | 检测值是75%-90%的颜色                                  |
| lowColor                | int       | 设置值低于75%的颜色                                    |

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

